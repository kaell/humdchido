package mx.com.soultech.humd.test

import org.scalatest.FunSuite
import mx.com.soultech.humd.impl.FSRepository
import java.io.File

class FSRepositoryTest extends FunSuite{
  
  test("Creating directory...") {
    var repo = new FSRepository
    val result = repo.createFolder("/Temp","TestDir")
    assert(result === true)
  }
  
  test("Deleting directory...") {
    var repo = new FSRepository
    val result = repo.deleteFolder("/Temp/TestDir")
    assert(result === true)
  }

}