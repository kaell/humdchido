package mx.com.soultech.humd

trait Searchable {
  def search(query:String, MetadataList:List[String]):List[Document]
  def validateQuery(query:String):Boolean
  def connect(connectionString: String, user: String, password: String): Boolean
  def checkin(document:Document):Boolean
  def getQuerysize(query:String): BigInt
}