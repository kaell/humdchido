package mx.com.soultech.humd.utils
import java.io._
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator
//import org.apache.log4j.xml.DOMConfigurator

object FillCombo {

  var lista: List[String] = Nil
  var listarepos: List[String] = Nil
  //var myArray: Array[Array[String]] = new Array[Array[String]](20, 5)

  var i = 0
  val logger = Logger.getLogger("FillCombo")
  PropertyConfigurator.configure("conf/log4j.properties")

  def llenacombo() {

    try {
      var lin = io.Source.fromFile("imagen/prueba.txt", "utf-8")
      var r = lin.getLines
      var linea = ""
      var prue = ""
      var res="" 
     while (r.hasNext) {
        linea = r.next()
        var cadenas = linea.split(',')
        if (cadenas(0) != "") {
          logger.info("Repositorio encontrado: " + cadenas(0) + "---" + cadenas(1))
          prue = cadenas(0) + "," + cadenas(1)
          res = cadenas(0)
          lista = lista :+ prue
          listarepos=listarepos :+ res
          i = i + 1

        }

        logger.info("Numero de Repositorio encontrados: " + i)

      }

      lin.close()

    } catch {
      case e: IOException => logger.error("El archivo no existe")
      case e =>
        logger.fatal("Tipo de Exepcion: " + e.printStackTrace())

    }

  }
}