package mx.com.soultech.humd.utils

import java.io.File
import java.io.BufferedReader
import java.io.PrintWriter
import java.io.FileReader
import java.io.FileWriter
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator
import java.awt.Desktop
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer
import java.io.IOException

object UtilFile {

  //var directorio:File=null
  //var listaDirectorio:List=Nail

  val directorio = new File("logs")
  var listaDirectorio = directorio.list()
  var tam = listaDirectorio.length
  if (tam < 1) {
    tam = 1
  }
  var myArrayBit: Array[Array[String]] = new Array[Array[String]](tam, 2);
  val logger = Logger.getLogger("UtilFile")
  PropertyConfigurator.configure("conf/log4j.properties")

  def removeLineFromFile(file: String, lineToRemove: String) {

    var r = 0;

    logger.info("Reading File...")

    val inFile = new File(file);

    if (!inFile.isFile()) {
      System.out.println("Parameter is not an existing file");
      logger.error("El Archivo no existe")

      return
    }

    //Construct the new file that will later be renamed to the original filename. 
    val tempFile = new File(inFile.getAbsolutePath() + ".tmp")
    val br = new BufferedReader(new FileReader(file));
    val pw = new PrintWriter(new FileWriter(tempFile));

    var line = ""
    var linea = ""

    val lin = io.Source.fromFile("imagen/prueba.txt", "utf-8")
    val ren = lin.getLines

    //Read from the original file and write to the new 
    //unless content matches data to be removed.
    while (ren.hasNext) {
      line = ren.next()

      //r=line.indexOf(",");
      //linea=line.substring(0,r);

      if (!line.startsWith(lineToRemove)) {

        pw.println(line);
        //System.out.println("Este es la linea:"+line);
        logger.info("Repositorio :" + line)
        pw.flush();
      }

    }
    pw.close();
    br.close();

    //Delete the original file

    lin.close()
    if (!inFile.delete()) {

      //System.out.println("?????????????????????Could not delete file");
      logger.error("No fue posible borrar el archivo")
      return ;
    }

    //Rename the new file to the filename the original file had.
    if (!tempFile.renameTo(inFile))

      logger.error("No es posible renombrar el archivo")
    System.out.println("Could not rename file")

    logger.info("nombre del repositorio borrado: " + lineToRemove)

  }

  def listaArchivos() {

    //var arrayfiles = new Array[String](2)
    var i = 0

    if (listaDirectorio == null)
      System.out.println("No hay ficheros en el directorio especificado");
    else {
      for (x <- 0 until listaDirectorio.length) {

        myArrayBit(i) = Array(listaDirectorio(x), "logs")

        // println("este es la lista: " + listaDirectorio(x))
        // println("este el arreglo: " + myArrayBit(i)(0))

        i += 1

      }

    }
    myArrayBit

  }

/* Metodo para abrir archivo log*/
  def openFile(fil: String) {

    try {

      val file2 = new File("logs/" + fil)
      Desktop.getDesktop().open(file2)
    } catch {
      case e: IOException => logger.error("No fue posible abrir el archivo selecionado" + e.printStackTrace())
      case e: Exception => logger.error("Excepcion de tipo: " + e.printStackTrace())

    }
  }

  def main(args: Array[String]) {
    UtilFile.listaArchivos()
    UtilFile.openFile("humd.log")

    // UtilFile.removeLineFromFile("imagen/prueba.txt", "prueba1");
  }

}
       
