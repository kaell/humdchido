package mx.com.soultech.humd.utils

import java.net.FileNameMap
import java.net.URLConnection
import java.io.InputStream
import java.io.FileOutputStream
import oracle.stellent.ridc.model.DataBinder
import oracle.stellent.ridc.IdcClientManager
import oracle.stellent.ridc.protocol.ServiceResponse
import oracle.stellent.ridc.IdcContext
import scala.collection.JavaConversions._
import oracle.stellent.ridc.IdcClient

object OracleUtils {
  
  // obtiene tipo mime de un archivo
  def getMimeType(MyFile:String) : String ={
	val fileNameMap:FileNameMap = URLConnection.getFileNameMap()
	fileNameMap.getContentTypeFor(MyFile)
  }
  
    // escribe archivo a disco
  private def use[T <: { def close(): Unit }](closable: T)(block: T => Unit) {
    try {
      block(closable)
    } finally {
      closable.close()
    }
  }

  // escribe archivo a disco
  def WriteToDisk(input: InputStream, path: String) {
    var success = true
    try{
    use(input) { in =>
      use(new FileOutputStream(path)) { out =>
        val buffer = new Array[Byte](1024)
        Iterator.continually(in.read(buffer))
          .takeWhile(_ != -1)
          .foreach { out.write(buffer, 0, _) }
      }
    }
    }catch{
      case _ => { success= false }
    }
    success
  }
  
  // Obtiene un databinder valido o null si ocurre un error
  def getDataBinder(IsConnected:Boolean, UCMManager : IdcClientManager) : DataBinder = {
    var databinder: DataBinder = null
    try{
      if(IsConnected){
        val MyUCMClient = UCMManager.getClient("MyClient")
        databinder = MyUCMClient.createBinder()
      }
    }catch {
      case _=> { databinder= null }
    }
    databinder
  }
  
  // executa un servicio devuelvel el databinder si todo fue correcto y null si ocurrio un error
  def execute(IsConnected:Boolean , UCMManager : IdcClientManager ,UCMclientcontext: IdcContext, databinder: DataBinder): ServiceResponse = {
    var response: ServiceResponse = null
    if (IsConnected) {
      try {
        val MyUCMClient = UCMManager.getClient("MyClient")
        response = MyUCMClient.sendRequest(UCMclientcontext, databinder)
      } catch {
        case _ => { response = null }
      }
    }
    response
  }
  
  // obtiene path de id (dCollectionID) regresa el path si todo fue correcto y null si no
  def getPath(IsConnected:Boolean, UCMManager : IdcClientManager,UCMclientcontext: IdcContext,dCollectionID: String): String = {
    var dpath = ""
    try {
      var databinder = getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "COLLECTION_GET_INFO")
      databinder.putLocal("hasCollectionID", "true")
      databinder.putLocal("dCollectionID", dCollectionID)
      val res = execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val path_parts = res.getResponseAsBinder().getResultSet("PATH")
      for (
        seccions <- path_parts.getRows();
        values <- seccions.entrySet() if (values.getKey().equals("dCollectionName"))
      ) {
        dpath += String.format("/%s", values.getValue())
      }
    } catch {
      case _ => { dpath = null }
    }
    dpath
  }
  
  // getDocID
  def getDocID(IsConnected:Boolean, UCMManager : IdcClientManager,UCMclientcontext: IdcContext,path: String, name: String): String = {
    var DocID = ""
    try {
      var databinder = getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "COLLECTION_GET_CONTENTS")
      databinder.putLocal("hasCollectionPath", "true")
      databinder.putLocal("dCollectionPath", path)
      val response = execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val resultset = response.getResponseAsBinder().getResultSet("CONTENTS")
      for(obj <- resultset.getRows()
          if(obj.get("dOriginalName").equals(name) && DocID.equals(""))
    		  ){
        DocID = obj.get("dID")
      }
    } catch {
      case _ => { DocID = null }
    }
    DocID
  }
  
}