package mx.com.soultech.humd.utils

import com.documentum.fc.client.IDfSession
import com.documentum.fc.client.IDfFolder
import com.documentum.com.DfClientX
import java.util.Properties
import java.io.FileInputStream
import java.io.FileOutputStream
import com.documentum.fc.client.IDfCollection
import com.documentum.fc.client.DfQuery
import com.documentum.fc.client.IDfQuery

object EmcUtils {

  // Exec DQL Query
  def execQuery(session: IDfSession, queryString: String): IDfCollection = {
    var results: IDfCollection = null
    try {
      var queryobj = new DfQuery()
      queryobj.setDQL(queryString)
      results = queryobj.execute(session, IDfQuery.DF_READ_QUERY);
    } catch {
      case _ => { results = null }
    }
    results
  }

  
  // Obtiene el path de un i_folder_id
  def getPathByID(DCTMSession: IDfSession, FolderID: String): String = {
    var path = ""
    val query = String.format("dm_folder where r_object_id='%s'", FolderID)
    try {
      val sobj = DCTMSession.getObjectByQualification(query).asInstanceOf[IDfFolder]
      path = sobj.getFolderPath(0)
    } catch {
      case _ => { path = null }
    }
    path
  }

  // obtiene el content type de la ruta de archivo pasada como parametro
  def getContentType(DCTMSession: IDfSession, FileName: String): String = {
    var clientx = new DfClientX()
    var format: String = null
    try {
      var oFormatRec = clientx.getFormatRecognizer(DCTMSession, FileName, null)
      format = oFormatRec.getDefaultSuggestedFileFormat()
    } catch {
      case _ => { format = null }
    }
    format
  }

  // Escribe properties documentum desde el classpath
  def setConnectionStringEmc(host: String, port: String): Boolean = {
    var success = true
    try {
      val dfcp = this.getClass().getClassLoader().getResource("dfc.properties")
      var path = dfcp.getFile().replace("/", "\\")
      path = path.substring(1, path.size)
      var props = new Properties()
      props.load(new FileInputStream(path))
      props.setProperty("dfc.docbroker.host[0]", host)
      props.setProperty("dfc.docbroker.port[0]", port)
      props.store(new FileOutputStream(path), null)
    } catch {
      case _ => { success = false }
    }
    success
  }
}