package mx.com.soultech.humd.impl

import java.io.IOException
import mx.com.soultech.humd.Document
import mx.com.soultech.humd.Repository
import scalax.file.Path
import java.io.File
import scala.io.Source
import scala.util.parsing.json.JSON._
import scala.collection.Map


class FSRepository extends Repository {

  def createFolder(path: String, name:String): Boolean = {
    try {
      val filePath: Path = Path.fromString(path + "\\" + name)
      filePath.createDirectory(createParents = true, failIfExists = true)
    } catch {
      case ioe: IOException => {
        print("Error al crear directorio " + path + ":" + ioe.getMessage())
        false //TODO Loguear errores.
      }
    }
    true
  }

  def deleteFolder(path: String): Boolean = {
    try {
      val filePath: Path = Path.fromString(path)
      filePath.deleteIfExists()
    } catch {
      case ioe: IOException => {
        print("Error al eliminar directorio " + path + ":" + ioe.getMessage())
        false  //TODO Loguear errores.
      }
    }
    true
  }
  
  def checkout(document:Document, path:String):Boolean = {
    var success = true
    try{
        val json_text = Source.fromFile(path).mkString
        parseFull(json_text) match {
    	case Some(jobject) => {
    			for( (k,v) <- jobject.asInstanceOf[Map[_,_]]){
    			  if(v.isInstanceOf[Map[_,_]]){
    			    for ((k,v) <- v.asInstanceOf[Map[_,_]]){
    			      document.customAttributes.put(k.toString, v.toString)
    			    }
    			  }else{
    				  k match {
    				    case "Path" => { document.Path = v.toString }
    				    case "Name" => { document.Name = v.toString }
    				    case "Type" => { document.Type = v.toString }
    				    case "contentPath" => { document.contentPath = v.toString }
    				    case "IsDirectory" => { document.IsDirectory = v.toString.toBoolean }
    				  }
    			  }
    			}
    		}
    	case _ => ()
       }
    }catch {
      case _ => { success = false }
    }
    success
  }

  def getDocuments(path: String): List[Document] = {
    var Docs: List[Document] = null
    var Doc: Document = null
    try {
      Docs = List[Document]()
      for (f <- new File(path).listFiles() if (f.getName().contains(".json"))) {
    	  Doc = new Document(null,null,null,false)
    	  checkout(Doc,f.getAbsolutePath())
    	  Docs ::= Doc
      }
    } catch {
      case _ => { Docs = null}
    }
    Docs
  }
  
  // get free space
  def getFreeSpace(path:String): BigInt = {
    var freespace : BigInt = -1 
    try{
    val fil = new File (path)
    freespace = fil.getFreeSpace()
    freespace = { if(freespace<1) -1 else freespace }
    }catch{
      case _=> {freespace = -1 }
    }
    freespace
  }
  
}