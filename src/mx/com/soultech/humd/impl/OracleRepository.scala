package mx.com.soultech.humd.impl

import mx.com.soultech.humd._
import oracle.stellent.ridc.IdcClientManager
import oracle.stellent.ridc.IdcClient
import oracle.stellent.ridc.IdcContext
import oracle.stellent.ridc.IdcClientException
import oracle.stellent.ridc.IdcClientConfig
import oracle.stellent.ridc.model.DataBinder
import oracle.stellent.ridc.protocol.ServiceResponse
import oracle.stellent.ridc.protocol.ServiceException
import oracle.stellent.ridc.model.TransferFile
import scala.collection.JavaConversions._
import java.io.File
import java.io.InputStream
import java.io.FileOutputStream
import java.net.FileNameMap
import java.net.URLConnection
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator
import mx.com.soultech.humd.utils.OracleUtils

import oracle.stellent.ridc.auth.impl.BasicCredentials

class OracleRepository extends Repository with Searchable {
  val logger = Logger.getLogger("class: OracleRepository")
  PropertyConfigurator.configure("conf/log4j.properties")
  private var UCMManager : IdcClientManager = null
  private var UCMclientcontext: IdcContext = null
  private var IsConnected : Boolean = false

  // Conecta a UCM
  def connect(connectionString: String, user: String, password: String): Boolean = {
    if (!IsConnected) {
      try {
        UCMManager = new IdcClientManager()
        var MyUCMclient = UCMManager.createClient(connectionString)
        UCMManager.addClient("MyClient", MyUCMclient)
        //val credentials = new BasicCredentials(user, password)
        UCMclientcontext = new IdcContext(user,password)
        // Setup the request
        val req = MyUCMclient.createBinder()
        req.putLocal("IdcService", "PING_SERVER")
        // Get the response
        val resp = MyUCMclient.sendRequest(UCMclientcontext, req)
        resp.getResponseAsBinder()// si resp es null, nullpointerexception connect = false
        //resp.getLocal("StatusMessage")// si el login no es correcto es null y lanza nullpointerexception
        IsConnected = true
      } catch {
        case _ => { IsConnected = false 
        			logger.warn("No fue posible realizar la conexion")
        }
      }
    }
    
    logger.info("La conexion se realizo de manera exitosa")
    IsConnected
  }

  // crea folder en ucm
  def createFolder(Path: String, Name:String): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "COLLECTION_ADD")
      databinder.putLocal("dCollectionName", Name)
      databinder.putLocal("hasParentCollectionPath", "true")
      databinder.putLocal("dParentCollectionPath", Path)
      databinder.putLocal("dCollectionOwner", UCMclientcontext.getUser())
      OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
    } catch {
      case _ => { success = false }
    }
    success
  }

  // borra folder de ucm
  def deleteFolder(Path: String): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "COLLECTION_INFO")
      databinder.putLocal("hasCollectionPath", "true")
      databinder.putLocal("dCollectionPath", Path)
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      //-------------------
      val resultset = res.getResponseAsBinder().getResultSet("PATH")
      val Data = resultset.getRows().get(resultset.getRows().size() - 1)
      val dGUID = Data.get("dCollectionGUID")
      //--------------------
      //databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.getLocalData().clear()
      databinder.putLocal("IdcService", "COLLECTION_DELETE")
      databinder.putLocal("hasCollectionGUID", "true")
      databinder.putLocal("dCollectionGUID", dGUID)
      databinder.putLocal("deleteImmediate", "true")
      databinder.putLocal("force", "true")      
      OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
    } catch {
      case _ => { success = false }
    }
    success
  }

  // devuelve una lista de valores o null si no ahi resultados
  def search(query: String, MetadataList: List[String] = null): List[Document] = {
    var Doc: Document = null
    var Docs:List[Document] = null
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "GET_SEARCH_RESULTS")
      databinder.putLocal("SearchEngineName", "database")
      databinder.putLocal("QueryText", query)
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val resulset = res.getResponseAsBinder().getResultSet("SearchResults")
      Docs = List[Document]()
      for (values <- resulset.getRows()) {
        Doc = new Document(OracleUtils.getPath(IsConnected,UCMManager,UCMclientcontext,values.get("xCollectionID")), values.get("dOriginalName"),values.get("dDocType"), false)
        // custom metadata Add
        if (MetadataList != null && !MetadataList.isEmpty)
          for (MetadataItem <- MetadataList)
            if(values.containsKey(MetadataItem)) Doc.customAttributes.put(MetadataItem, values.get(MetadataItem))
        Docs ::= Doc
      }
    } catch {
      case _ => { Docs = null }
    }
    Docs
  }

  // true si la consulta arroja resultados false si no
  def validateQuery(query: String): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "GET_SEARCH_RESULTS")
      databinder.putLocal("SearchEngineName", "database")
      databinder.putLocal("QueryText", query)
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val resulset = res.getResponseAsBinder().getResultSet("SearchResults")
    } catch {
      case _ => { success = false }
    }
    success
  }

  // descarga un archivo
  def checkout(document: Document, path: String): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "GET_FILE")
      databinder.putLocal("dID", OracleUtils.getDocID(IsConnected,UCMManager,UCMclientcontext,document.Path, document.Name))
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val filestream = res.getResponseStream()
      OracleUtils.WriteToDisk(filestream, path + "\\" + document.Name)
      document.contentPath = path + "\\" + document.Name
      // save JSON Document
      val gson_path = String.format("%s\\%s.json", path, document.Name)
      val pw = new java.io.PrintWriter(gson_path)
      pw.write(document.toJSON)
      pw.close()
    } catch {
      case _ => { success = false }
    }
    success
  }
  
  // sobrecarga de checkout para que reciva el dID como parametro , posible Mejora ver despues
  def checkout(document: Document, path:String, dID:String): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "GET_FILE")
      databinder.putLocal("dID", dID)
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val filestream = res.getResponseStream()
      OracleUtils.WriteToDisk(filestream, path + "\\" + document.Name)
      document.contentPath = path + "\\" + document.Name
    } catch {
    	case _ => { success = false }
    }
    success
  }

  def checkin(document: Document): Boolean = {
    var success = true
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "COLLECTION_INFO")
      databinder.putLocal("hasCollectionPath", "true")
      databinder.putLocal("dCollectionPath", document.Path)
      //------------------------------------------
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val resultset = res.getResponseAsBinder().getResultSet("PATH")
      val Data = resultset.getRows().get(resultset.getRows().size() - 1)
      val dCollectionID = Data.get("dCollectionID")
      // ---------- CheckIN ----------------------
      databinder.getLocalData().clear()
      databinder.putLocal("IdcService", "CHECKIN_UNIVERSAL")
      databinder.putLocal("xCollectionID", dCollectionID)
      databinder.putLocal("dFormat", OracleUtils.getMimeType(document.contentPath))
      databinder.putLocal("dDocAuthor", UCMclientcontext.getUser())
      databinder.putLocal("dSecurityGroup", "public")
      databinder.putLocal("dDocType", document.Type)
      //--- Add custom Metadata ----------
      if (!document.customAttributes.isEmpty)
        for ((key, value) <- document.customAttributes)
          databinder.putLocal(key, value)
      //--- Add content file -------------
      databinder.addFile("primaryFile", new TransferFile(new File(document.contentPath)));
      OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
    } catch {
      //case _ => { success = false }
      case e:Exception => throw new Error("Este es el error"+e.printStackTrace())
    }
    success
  }
  
   def getQuerysize(query:String): BigInt = {
     var result:BigInt = 0
    try {
      var databinder = OracleUtils.getDataBinder(IsConnected,UCMManager)
      databinder.putLocal("IdcService", "GET_SEARCH_RESULTS")
      databinder.putLocal("SearchEngineName", "database")
      databinder.putLocal("QueryText", query)
      val res = OracleUtils.execute(IsConnected,UCMManager,UCMclientcontext,databinder)
      val resulset = res.getResponseAsBinder().getResultSet("SearchResults")
      for (values <- resulset.getRows()) {
        result += values.getInteger("VaultFileSize")
      }
    } catch {
      case _ => { result = -1 } 
    }
    result
   }
  
}