package mx.com.soultech.humd.impl

import mx.com.soultech.humd._
import com.documentum.fc.client.IDfClient
import com.documentum.fc.common.IDfLoginInfo
import com.documentum.fc.client.IDfSession
import com.documentum.fc.client.DfClient
import com.documentum.fc.common.DfLoginInfo
import com.documentum.fc.client.IDfFolder
import com.documentum.operations.IDfDeleteOperation
import com.documentum.com.DfClientX
import com.documentum.fc.common.DfException
import com.documentum.fc.client.IDfSysObject
import com.documentum.fc.client.DfQuery
import com.documentum.fc.client.IDfCollection
import com.documentum.fc.client.IDfQuery
import com.documentum.fc.client.IDfDocument
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator

//import scala.collection.JavaConversions._

import java.io.File
import mx.com.soultech.humd.utils.EmcUtils

class EmcRepository extends Repository with Searchable {
  
  val logger = Logger.getLogger("class: EmcRepository")
  PropertyConfigurator.configure("conf/log4j.properties")
  private var DCTMClient: IDfClient = null
  private var DCTMLogin: IDfLoginInfo = null
  private var DCTMSession: IDfSession = null
  private var IsConnected: Boolean = false

  /*
   *DfException */

  

  def connect(connectionString: String, user: String, password: String): Boolean = {
    try {
      val cs_p = connectionString.split(":")
      if(!EmcUtils.setConnectionStringEmc(cs_p(0),cs_p(1))) throw new Error("Error escribiendo dfc.properties.")
      DCTMClient = DfClient.getLocalClient()
      DCTMLogin = new DfLoginInfo()
      DCTMLogin.setUser(user)
      DCTMLogin.setPassword(password)
      DCTMLogin.setDomain("")
      DCTMSession = DCTMClient.newSession("documentum", DCTMLogin)
      IsConnected = true
    } catch {
      case _ => { IsConnected = false 
        
                 logger.error("No se pudo realizar la conexion")
    		  
      }
    
     logger.info("Se realizo la conexion")
    }
    IsConnected
     
  
  }

  def createFolder(path: String, name: String): Boolean = {
    var folder: IDfFolder = null
    var success = true
    try {
      folder = DCTMSession.newObject("dm_folder").asInstanceOf[IDfFolder]
      folder.setObjectName(name)
      folder.link(path)
      folder.save()
    } catch { 
      case _ => { success = false 
    		  	  logger.warn("El objeto no pudo ser creado")	
      
      }
    
        	logger.info("El objeto fue creado")
    
    }
    success
  }

  def deleteFolder(path: String): Boolean = {
    var folder: IDfFolder = null
    var clientx = new DfClientX()
    var delop: IDfDeleteOperation = null
    var success = true
    try {
      folder = DCTMSession.getObjectByPath(path).asInstanceOf[IDfFolder]
      delop = clientx.getDeleteOperation()
      delop.add(folder)
      delop.setDeepFolders(true)
      delop.execute()
    } catch { 
      case _ => { success = false  }
    }
    success
  }

  def search(query: String, MetadataList: List[String] = null): List[Document] = {
    var Doc: Document = null
    var Docs: List[Document] = null
    var TypesColl: IDfCollection = null
    var DocsColl: IDfCollection = null
    var fullquery: String = null
    try {
      var fullquery = String.format("select distinct r_object_type from dm_sysobject where %s", query)
      TypesColl = EmcUtils.execQuery(DCTMSession, fullquery)
      Docs = List[Document]()
      while (TypesColl.next()) {
        fullquery = String.format("select *,i_folder_id,r_full_content_size from %s where %s", TypesColl.getString("r_object_type"), query)
        DocsColl = EmcUtils.execQuery(DCTMSession, fullquery)
        while (DocsColl.next()) {
          if (DocsColl.getString("i_folder_id") != null) {
            // doc
            Doc = new Document(
              EmcUtils.getPathByID(DCTMSession, DocsColl.getString("i_folder_id")),
              DocsColl.getString("object_name"),
              TypesColl.getString("r_object_type"),
              false)
            // Search for metadata
            if (MetadataList != null && !MetadataList.isEmpty)
              for (MetadataItem <- MetadataList)
                if (DocsColl.hasAttr(MetadataItem)) Doc.customAttributes.put(MetadataItem, DocsColl.getString(MetadataItem))
            // Add    
            Docs ::= Doc
          }
        }
      }
    } catch {
      case e: Exception => {
        throw new Error("Este es el error" + e.printStackTrace())
        Docs = null
      }
    }
    Docs
  }

  def validateQuery(query: String): Boolean = {
    var sysobj: IDfSysObject = null
    var collection: IDfCollection = null
    var queryObj = new DfQuery
    var strquery = String.format("select r_object_id,object_name,i_folder_id from dm_document where %s", query)
    var success = true
    try {
      queryObj.setDQL(strquery)
      collection = queryObj.execute(DCTMSession, IDfQuery.DF_READ_QUERY)
      success = true
    } catch {
      case _ => { success = false 
        
    		   e:Exception => throw new Error("Este es el error"+e.printStackTrace())
      
      }
    }
    success
  }

  def checkout(document: Document, path: String): Boolean = {
    	var doc:IDfDocument  = null
		var success = true
		try {
			doc= DCTMSession.getObjectByPath(document.Path + "/" + document.Name).asInstanceOf[IDfDocument]
			if(doc!=null){
				if(!doc.isCheckedOut()){
					doc.checkout();
					doc.getFile(path + "\\" + document.Name)
					document.contentPath = path + "\\" + document.Name
					// save JSON document
					val gson_path = String.format("%s\\%s.json",path,document.Name)
					val pw = new java.io.PrintWriter(gson_path)
					pw.write(document.toJSON)
					pw.close()
				}
				doc.cancelCheckout()
			}
		} catch {
		  case _ => { 
		    
		    
		     e:Exception => throw new Error("Este es el error"+e.printStackTrace())
		    success = false }
		}	
		success
	}
  
  def checkin(document: Document): Boolean = {
    	var Doc:IDfDocument = null
		var success = true
		try {
			Doc= DCTMSession.newObject(document.Type).asInstanceOf[IDfDocument]
			Doc.setObjectName(document.Name)
			Doc.setContentType(EmcUtils.getContentType(DCTMSession,document.contentPath));
			Doc.setFile(document.contentPath)
			// add metadata
			if(!document.customAttributes.isEmpty)
				for ((key,value) <- document.customAttributes)
					Doc.setString(key,value)
			Doc.link(document.Path)
			Doc.save()
		} catch {
		  case _ => { success = false }
		
		      e:Exception => throw new Error("Este es el error"+e.printStackTrace())
		
		}
		success
  }
  
  // regresa tama�o en bytes del resultado del query, -1 si ahi error
   def getQuerysize(query:String): BigInt = {
     var size:BigInt = 0
     try{
      val fullquery = String.format("select SUM(r_full_content_size) as t_size from dm_sysobject where %s", query)
      val RColl = EmcUtils.execQuery(DCTMSession, fullquery)
      RColl.next()
      size = RColl.getInt("t_size")
     }catch{
       case _ => { size = -1 }
     }
     size
   }

}