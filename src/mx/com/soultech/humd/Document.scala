package mx.com.soultech.humd

import scala.collection.mutable.HashMap

class Document(cPath:String = null,cName:String = null,cType:String=null,IsDir:Boolean = false) {
  var Path : String = cPath
  var Name : String = cName
  var Type : String = cType
  var IsDirectory : Boolean = IsDir
  // path del contenido en disco
  var contentPath: String = null
  // Attributos extras
  var customAttributes = new HashMap[String,String]
  // procesa map a string correctamente
  private def MaptoString : String =  {
    var res = ""
    var i = 1
    if(customAttributes!=null && !customAttributes.isEmpty)
    for((key,value) <- customAttributes){
      val sep = if(i.equals(customAttributes.size)) "" else ", " 
      res += String.format(""""%s":"%s"%s""",key,value,sep)
      i += 1
    }
    res
  }
  // sobreescritura de tostring , ej: println(Document)
  override def toString = { "Document [Path="+Path+
    						", Name="+Name+ 
    						", Type="+Type+
    						", IsDirectory="+IsDirectory+ 
    						", contentPath="+contentPath+
    						", customAttributes="+customAttributes+"]" }
  // to JSON
  def toJSON : String = {
    String.format("""{"Path":"%s", "Name":"%s", "Type":"%s", "IsDirectory":"%s", "contentPath":"%s", "customAttributes":{%s}}""", 
        Path,
        Name,
        Type,
        IsDirectory.toString,
        (if(contentPath!=null) contentPath.replace("\\","/") else "null"),
        MaptoString
        )
  }
}