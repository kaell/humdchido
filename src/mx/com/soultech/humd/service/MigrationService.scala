package mx.com.soultech.humd.service

import mx.com.soultech.humd._
import mx.com.soultech.humd.impl.EmcRepository
import mx.com.soultech.humd.impl.OracleRepository
import mx.com.soultech.humd.impl.FSRepository
import org.apache.log4j.Logger
import org.apache.log4j.PropertyConfigurator
import java.io.File

object MigrationService {
  
  val logger = Logger.getLogger("FillCombo")
  PropertyConfigurator.configure("conf/log4j.properties")
  // Realiza migracion de filesystem a Content Server
  def FStoCS[T <: Searchable]( FS:FSRepository , path:String , CS:T, pathCS:String ){
    try{
     val spath:File = new File(path)
     val res = FS.getDocuments(path)
     if(!spath.exists() || !spath.isDirectory() || res==null  || res.isEmpty ) throw new Error("Error FStoCS")
	 for (Doc <- res) { // busqueda en UCM incluyendo metadatos
	   
	   logger.info("Documento migrado" + "--" +Doc)
	   Doc.Path = pathCS
	   
	   // Fix temporal por si los tipos documentales no existen
	   if(CS.isInstanceOf[EmcRepository])
	     Doc.Type = "dm_document"
	   println(Doc)
	   if(!CS.checkin(Doc)) throw new Error("Error checkin")
	 }
    }catch{
      case _=> throw new Exception("Error al realizar la migracion FStoCS.")
    }
  }
  
  // Realiza migracion de Content Server a filesystem
  def CStoFS[T <: Searchable ](CS:T, CSquery:String, CSMetadata:List[String], FSpath:String ){
      val res = CS.search(CSquery, CSMetadata) 

      if( res == null || res.isEmpty){
        throw new NullPointerException("Query sin resultados")
        println("El query ingresado no contiene resultado")
        logger.info("El query ingresado no contiene resultado")
        
      } 
	 for (Doc <- res){
         
	  logger.info("Documento migrado" + "--" +Doc) 
	   if(!CS.asInstanceOf[Repository].checkout(Doc, FSpath)) throw new Error("Error checkOut")
	 }
  }
}