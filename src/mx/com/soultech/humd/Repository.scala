package mx.com.soultech.humd

abstract class Repository {
  def createFolder(path:String, Name:String):Boolean
  def deleteFolder(path:String):Boolean
  def checkout(document:Document, path:String):Boolean
}
