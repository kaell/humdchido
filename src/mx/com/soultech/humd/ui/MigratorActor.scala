package mx.com.soultech.humd.ui

import scala.actors._
import mx.com.soultech.humd.impl.OracleRepository
import mx.com.soultech.humd.service.MigrationService

case class MigrateCSToFS(contentType:String, url:String, user:String, pass:String, query:String, path:String, atributeList:List[String]) {}

class MigratorActor extends Actor {
  def act = {
    loop {
      react {
        case MigrateCSToFS(contentType, url, user, pass, query, path, atributeList) =>
          if (contentType == "Oracle") {
            try {
              val repositorioOri: OracleRepository = new OracleRepository
              if (!repositorioOri.connect(url, user, pass))
                throw new IllegalArgumentException("Error al conectar" + contentType)

              MigrationService.CStoFS(repositorioOri, query, atributeList, path)

              println("Migracion correcta Documetum CS a FS")
              sender ! "Migración finalizada"
            } catch {

              case a: IllegalArgumentException => {
                println("Error al ")
                a.printStackTrace()
                //ErroronnetDialog()
              }

              case e: NullPointerException => {
                println("ACA vemos que ec")
                e.printStackTrace()

                println("ACA vemos que ec")
                //ErrorNullQuery()
              }
              case _ => {
                //ErrorFSCSDialog()
              }
            }
          }
      }
    }
  }
}