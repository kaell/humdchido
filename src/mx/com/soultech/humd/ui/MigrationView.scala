package mx.com.soultech.humd.ui

import scala.swing._
import javax.swing.ComboBoxEditor
import scala.swing.ComboBox
import javax.swing.JFileChooser
import swing.event._
import java.awt.Dimension
import scala.io.Source
import scala.actors._
import java.io._
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
import mx.com.soultech.humd.utils.FillCombo
import mx.com.soultech.humd.impl.OracleRepository
import mx.com.soultech.humd.impl.EmcRepository
import mx.com.soultech.humd.utils.FillTables
import mx.com.soultech.humd.utils.Intermediator
import mx.com.soultech.humd.service.MigrationService
import mx.com.soultech.humd.impl.FSRepository
import mx.com.soultech.humd.utils.Intermediator
import mx.com.soultech.humd.utils.Receive
import mx.com.soultech.humd.utils.Send

object MigrationView extends SimpleSwingApplication {

  var user = ""
  var pass = ""
  var url = ""
  var tipo = ""
  var tipo2 = ""
  var ruta = ""
  var query = ""
  var option = ""
  var indice = 0
  var indice2 = 0
  var titulo = ""
  var mensage = ""
  var atrilist = ""
  var atributeList: List[String] = Nil

  FillTables.llenaTablas()
  val listcombo = FillCombo.lista
  val radioButtonM1 = new RadioButton {

    action = Action("De FS a CS") {

      tfspath2.visible = false
      buttonopen2.visible = true
      buttonopen3.visible = false
      buttonopen.visible = false
      tfspath.visible = true
      combo.visible = false
      combo2.visible = true
      destinotemplabel.visible = false
      textfieldori3.visible = false
      textfieldrutaor.visible = true
      textfieldrutaor.text = ""
      tfspath.text = ""
      textfieldquery.visible = false
      textfieldquery.text = "Escriba su query..."
      buttonconfir.visible = true
      atributlis.visible = false
      option = "De FS a CS"

    }

  }

  val radioButtonM2 = new RadioButton() {

    action = Action("De CS a FS") {

      combo.visible = true
      tfspath2.visible = true
      tfspath.visible = false
      destinotemplabel.visible = false
      buttonopen.visible = true
      buttonopen2.visible = false
      buttonopen3.visible = false
      combo2.visible = false
      textfieldori3.visible = false
      textfieldrutaor.text = ""
      textfieldrutaor.visible = false
      textfieldrutades.text = ""
      tfspath2.text = ""
      textfieldquery.text = "Escriba su query..."
      textfieldquery.visible = true
      buttonconfir.visible = true
      atributlis.visible = true
      option = "De CS a FS"
    }

  }

  val radioButtonM3 = new RadioButton {

    action = Action("De CS a CS") {

      combo.visible = true
      tfspath2.visible = false
      tfspath.visible = false
      buttonopen.visible = false
      buttonopen2.visible = false
      combo2.visible = true
      destinotemplabel.visible = true
      textfieldori3.visible = true
      buttonopen3.visible = true
      textfieldrutaor.visible = true
      textfieldrutaor.text = ""
      textfieldrutades.text = ""
      textfieldori3.text = ""
      textfieldquery.text = "Escriba su query..."
      buttonconfir.visible = true
      textfieldquery.visible = true
      atributlis.visible = true
      option = "De CS a CS"
    }

  }

  val buttonopen = new Button("Examinar") {
    visible = false
  }

  val buttonhelp = new Button(" Ayuda ")

  val buttonopen2 = new Button("Examinar") {
    visible = false
  }

  val buttonopen3 = new Button("Examinar") {
    visible = false
  }

  val buttoncancel = new Button("Cancelar")

  val buttonconfir = new Button("Confirmar") {
    visible = false
  }

  FillCombo.llenacombo()
  val combo = new ComboBox(listcombo) {
    visible = false
  }

  val combo2 = new ComboBox(listcombo) {
    visible = false
  }

  val opcionlabel = new Label("2. Seleccione Origen y Destino")
  val orinlabel = new Label("Ruta Origen: ")

  val tfspath2 = new TextArea {
    //columns = 15
    //rows = 5
    visible = false
  }

  val tfspath = new TextArea {
    //columns = 5
    //rows = 5
    visible = false
  }
  val textfieldori3 = new TextArea {
    //columns = 5
    //rows = 5
    visible = false
  }

  val textfieldrutaor = new TextField {
    columns = 15
    visible = false

  }

  val ori = new Label("Origen :  ")

  val ori2 = new Label("Origen de datos en FS:  ") {
    visible = false
  }

  val ori3 = new Label("Origen de datos en el CS:  ") {
    visible = false
  }

  val textfieldrutades = new TextField {
    columns = 15
    //rows=20
    //visible = false

  }
  val destinolabel = new Label("Ruta Destino: ") {

    //visible = false
  }

  val destinotemplabel = new Label("Destino Temporal: ") {

    visible = false
  }

  val destinolabel2 = new Label("Destino: ")

  val avanzadolabel = new Label("4.Configuracion  Avanzada ")

  val textfieldquery = new TextArea("Escriba su Query...    ") {
    rows = 10
    lineWrap = true
    wordWrap = true
    visible = false
  }

  val atributlis = new TextArea("Escriba la lista de atributos...    ") {
    rows = 10
    lineWrap = true
    wordWrap = true
    visible = false
  }

  val rateslabel = new Label("3. Escriba las Rutas")

  val progressBar = new ProgressBar() {
    indeterminate = true
    visible = false
  }

  val top = new MainFrame {
    frame =>
    title = "Migrar"
    preferredSize = new Dimension(620, 740)
    iconImage = toolkit.getImage("imagen/logocorto.png")
    size = new Dimension(200, 200)
    visible = true

    contents = new BoxPanel(Orientation.Vertical) {

      val buttons = new ButtonGroup(radioButtonM1, radioButtonM2, radioButtonM3)

      contents += new BorderPanel() {

        val tip = new Label("1. Tipo de Migracion: ")
        add(tip, BorderPanel.Position.West)

      }

      contents += new BoxPanel(Orientation.Horizontal) {

        //contents += new Label("1. Tipo de Migracion: ")
        border = Swing.EmptyBorder(20, 5, 10, 30)
        contents += radioButtonM1
        contents += Swing.HStrut(20)
        contents += radioButtonM2
        contents += Swing.HStrut(30)
        contents += radioButtonM3
        contents += Swing.HStrut(30)
        //contents += helpbox

      }

      contents += new BorderPanel {
        //border = Swing.EmptyBorder(20, 5, 10, 30)
        add(opcionlabel, BorderPanel.Position.West)

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 5, 10, 40)
        contents += ori
        contents += combo
        contents += tfspath
        contents += buttonopen2
      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        contents += destinotemplabel
        contents += textfieldori3
        contents += buttonopen3
        contents += combo2

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        contents += destinolabel2
        contents += tfspath2
        contents += combo2
        contents += buttonopen
      }

      contents += new BorderPanel {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        add(rateslabel, BorderPanel.Position.West)

      }

      contents += new BorderPanel {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        add(orinlabel, BorderPanel.Position.West)
        add(destinolabel, BorderPanel.Position.East)

      }

      contents += new BorderPanel {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        add(textfieldrutaor, BorderPanel.Position.West)
        add(textfieldrutades, BorderPanel.Position.East)
      }
      contents += new BorderPanel {
        border = Swing.EmptyBorder(20, 5, 10, 30)
        add(avanzadolabel, BorderPanel.Position.West)

      }

      contents += new BoxPanel(Orientation.Vertical) {
        contents += Swing.VStrut(5)
        contents += new Label("Query")
        contents += Swing.VStrut(3)
        contents += new ScrollPane(textfieldquery)
        contents += Swing.VStrut(5)
        contents += new Label("Lista de Atributos")
        contents += new ScrollPane(atributlis)
        contents += Swing.VStrut(5)
      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)
        contents += Swing.HStrut(20)
        contents += buttoncancel
        contents += Swing.HStrut(20)
        contents += buttonhelp
        contents += Swing.HStrut(20)
        contents += buttonconfir
        contents += Swing.HStrut(20)
        contents += progressBar
      }

      listenTo(buttoncancel)
      reactions += {
        case ButtonClicked(`buttoncancel`) => showCloseDialog()

      }

      listenTo(buttonhelp)
      reactions += {
        case ButtonClicked(`buttonhelp`) => helpDialog()

      }

      listenTo(buttonconfir)
      reactions += {
        case ButtonClicked(`buttonconfir`) => {

          if (option == "De FS a CS") {
            if (textfieldrutaor.text == "" || textfieldrutades.text == "") {
              incompleteDatDialog()
              frame.close
            } else
              showConfirDialog()
          }

          if (option == "De CS a FS") {
            if (textfieldrutades.text == "") {
              incompleteDatDialog()
              frame.close
            } else
              showConfirDialog()
          }

          if (option == "De CS a CS") {
            if (textfieldrutaor.text == "" || textfieldrutades.text == "") {
              incompleteDatDialog()
              frame.close
            } else
              showConfirDialog()
          }

        }

      }

      listenTo(buttonopen)
      reactions += {
        case ButtonClicked(`buttonopen`) =>

          val fc = new JFileChooser()
          fc.setDialogTitle("Ubicacion del archivo resultante")
          fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

          val returnVal = fc.showOpenDialog(null)
          if (returnVal == JFileChooser.APPROVE_OPTION) {
            val file = fc.getSelectedFile()
            ruta = file.getAbsoluteFile().toString
            tfspath2.append(ruta)
            textfieldrutades.text = ruta
          } else {
            print("Comando cancelado por el usuario.")
          }

      }

      listenTo(buttonopen2)
      reactions += {
        case ButtonClicked(`buttonopen2`) =>
          val fc = new JFileChooser()
          fc.setDialogTitle("Ubicacion del archivo resultante")
          fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

          val returnVal = fc.showOpenDialog(null)
          if (returnVal == JFileChooser.APPROVE_OPTION) {
            val file = fc.getSelectedFile()

            ruta = file.getAbsoluteFile().toString
            tfspath.append(ruta)
            textfieldrutaor.text = ruta
          } else {
            print("Comando cancelado por el usuario.")
          }

      }

      listenTo(buttonopen3)
      reactions += {
        case ButtonClicked(`buttonopen3`) =>

          val fc = new JFileChooser()
          fc.setDialogTitle("Ubicacion del archivo resultante")
          fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

          val returnVal = fc.showOpenDialog(null)
          if (returnVal == JFileChooser.APPROVE_OPTION) {
            val file = fc.getSelectedFile()
            ruta = file.getAbsoluteFile().toString
            textfieldori3.append(ruta)
            textfieldrutaor.text = ruta
          } else {
            print("Comando cancelado por el usuario.")
          }

      }

    }

    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    private def showCloseDialog() {
      Dialog.showConfirmation(parent = null,
        title = "Cancel",
        message = "�Esta seguro que quiere salir de Migrar?") match {
          case Dialog.Result.Ok => {
            frame.close()
          }
          case _ => ()
        }
    }

    private def showConfirDialog() {

      var selec = ""
      titulo = "Confirmacion de los datos"
      query = textfieldquery.text
      atrilist = atributlis.text

      if (option == "De FS a CS") {

        tipo = "FileSystem"
        selec = combo2.selection.item
        indice = listcombo.indexOf(selec)

        mensage = "Los datos son correctos? " + "\n" + "Tipo de Migracion: " + option + "\n" + "ruta origen en el FS: " + textfieldrutaor.text + "\n" + "Repositorio Destino: " + selec + "\n" + "ruta destino en el repositorio: " + textfieldrutades.text + "\n"

      } else if (option == "De CS a FS") {

        selec = combo.selection.item
        indice = listcombo.indexOf(selec)
        mensage = "Los datos son correctos? " + "\n" + "Tipo de Migracion: " + option + "\n" + "Repositorio  origen : " + selec + "\n" + "Ruta de origen en el Repositorio: " + textfieldrutaor.text + "\n" + "Ruta destino en el FS: " + textfieldrutades.text + "\n" + "query: " + query + "\n"

      } else if (option == "De CS a CS") {

        selec = combo.selection.item
        var selec2 = combo2.selection.item
        indice = listcombo.indexOf(selec)
        indice2 = listcombo.indexOf(selec2)
        //println("Esta es el indice: " + indice)
        //println("Esta es el indice2: " + indice2)

        mensage = "Los datos son correctos? " + "\n" + "Tipo de Migracion: " + option + "\n" + "Repositorio  origen : " + selec + "\n" + "Destino temporal en el FS" + textfieldori3.text + "\n" + "Repositorio Destino " + selec2 + "\n" + "Ruta de origen : " + textfieldrutaor.text + "\n" + "Ruta destino: " + textfieldrutades.text + "\n" + "query: " + query + "\n"

      }

      tipo = FillTables.myArray(indice)(1)
      tipo2 = FillTables.myArray(indice2)(1)
      user = FillTables.myArray(indice)(4)
      pass = FillTables.myArray(indice)(5)
      url = FillTables.myArray(indice)(2) + ":" + FillTables.myArray(indice)(3)
      if (tipo == "Oracle") {
        url = FillTables.myArray(indice)(2) + ":" + FillTables.myArray(indice)(3) + "/cs/idcplg"
      }

      Dialog.showConfirmation(parent = null, title = titulo, message = mensage) match {

        case Dialog.Result.Ok => {

          println("OK")

          if (option == "De FS a CS") {

            println("FileSystem")

            if (tipo.equals("Documentum")) {

              try {

                var repositorioOri: FSRepository = new FSRepository
                var repositorioDes: EmcRepository = new EmcRepository

                if (!repositorioDes.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)

                //println("datos" + textfieldrutaor.text + "----" + textfieldrutades.text)
                MigrationService.FStoCS(repositorioOri, textfieldrutaor.text, repositorioDes, textfieldrutades.text)

                ExitoDialog()
                sys.exit()
              } catch {
                case a: IllegalArgumentException => {
                  println("Error al ")
                  a.printStackTrace()
                  ErroronnetDialog()
                }

                case e: NullPointerException => {
                  println("ACA vemos que ec")
                  e.printStackTrace()

                  println("ACA vemos que ec")
                  ErrorNullQuery()
                  //ErrorFSCSDialog()

                }

                case a: Error => {
                  println("Error al ")
                  a.printStackTrace()

                }

                case _ => {

                  ErrorFSCSDialog()
                  sys.exit()

                }

              }

            }

            if (tipo.equals("Oracle")) {

              try {

                var repositorioOri: FSRepository = new FSRepository
                var repositoriodes: OracleRepository = new OracleRepository
                if (!repositoriodes.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)
                MigrationService.FStoCS(repositorioOri, textfieldrutaor.text, repositoriodes, textfieldrutades.text)
                println("Migracion Exitosa")
                ExitoDialog()
                sys.exit()

              } catch {

                case e: Exception => {

                  e.printStackTrace()

                  ErrorFSCSDialog()

                }

                case _ => {

                  ErrorFSCSDialog()
                  sys.exit()

                }
              }

            }
          }

          if (option == "De CS a FS") {

            fillAtribList()
            var reposides: FSRepository = new FSRepository

            if (tipo.equals("Documentum")) {
              println("Documentum")

              try {
                println("Haciendo conexion")
                var repositoroemc: EmcRepository = new EmcRepository
                if (!repositoroemc.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)
                //println("datos: url---" + url + "user---" + user + "pass---" + pass + "++++++++++++" + textfieldrutades.text)

                MigrationService.CStoFS(repositoroemc, query, null, textfieldrutades.text)
                println("Migracion correcta Documetum CS a FS")
                ExitoDialog()
                sys.exit()
              } catch {

                case a: IllegalArgumentException => {
                  println("Error al ")
                  a.printStackTrace()
                  ErroronnetDialog()
                }

                case e: NullPointerException => {
                  println("ACA vemos que ec")
                  e.printStackTrace()

                  println("ACA vemos que ec")
                  ErrorNullQuery()
                  //	ErrorFSCSDialog()

                }

                case _ => {

                  ErrorFSCSDialog()
                  //sys.exit()

                }

              }
            }

            if (tipo.equals("Oracle")) {
              Send(MigrateCSToFS("Oracle", url, user, pass, query, textfieldrutades.text, atributeList))
              progressBar.visible = true
            }

          } else if (option == "De CS a CS") {

            fillAtribList()
            var reposora: OracleRepository = new OracleRepository
            var reposfs: FSRepository = new FSRepository
            var reposemc: EmcRepository = new EmcRepository
            var user2 = FillTables.myArray(indice2)(4)
            var pass2 = FillTables.myArray(indice2)(5)
            var url2 = FillTables.myArray(indice2)(2) + ":" + FillTables.myArray(indice2)(3)
            if (tipo2 == "Oracle") {
              url2 = url2 + "/cs/idcplg"
            }

            if (tipo == "Oracle") {

              if (tipo2 == "Oracle") {

                try {

                  if (!reposora.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)
                  //var reposorac: OracleRepository = new OracleRepository
                  MigrationService.CStoFS(reposora, query, atributeList, textfieldori3.text)
                  if (!reposora.connect(url2, user2, pass2)) throw new IllegalArgumentException("Error al conectar" + tipo2)

                  MigrationService.FStoCS(reposfs, textfieldrutaor.text, reposora, textfieldrutades.text)
                  ExitoCSCSDialog()
                  sys.exit()

                } catch {

                  case a: IllegalArgumentException => {
                    println("Error al ")
                    a.printStackTrace()
                    ErroronnetDialog()
                  }

                  case e: NullPointerException => {
                    println("ACA vemos que ec")
                    e.printStackTrace()

                    println("ACA vemos que ec")
                    ErrorNullQuery()

                  }

                  case _ => {

                    ErrorCSCSDialog()
                    sys.exit()

                  }
                }

              }

              if (tipo2 == "Documentum") {

                try {

                  println("Datos url" + url, "user" + user + "paas" + pass)
                  if (!reposora.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)

                  MigrationService.CStoFS(reposora, query, atributeList, textfieldori3.text)

                  //println("Datos url2" + url2, "user2" + user2 + "paas2" + pass2)
                  if (!reposemc.connect(url2, user2, pass2)) throw new IllegalArgumentException("Error al conectar" + tipo2)
                  MigrationService.FStoCS(reposfs, textfieldrutaor.text, reposemc, textfieldrutades.text)
                  ExitoCSCSDialog()
                  sys.exit()

                } catch {

                  case a: IllegalArgumentException => {
                    println("Error al ")
                    a.printStackTrace()
                    ErroronnetDialog()
                  }

                  case e: NullPointerException => {
                    println("ACA vemos que ec")
                    e.printStackTrace()

                    println("ACA vemos que ec")
                    ErrorNullQuery()
                    //ErrorFSCSDialog()

                  }

                  case _ => {

                    ErrorCSCSDialog()
                    sys.exit()

                  }

                }

              }

            }

            if (tipo == "Documentum") {

              if (tipo2 == "Oracle") {

                println("De Docu a ORa")
                try {
                  var reposorac: OracleRepository = new OracleRepository

                  if (!reposemc.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)
                  //println("URL: " + url + "user " + user + "pass" + pass)
                  //println("datos query: " + query + "ruta" + textfieldori3.text)
                  MigrationService.CStoFS(reposemc, query, null, textfieldori3.text)

                  if (!reposorac.connect(url2, user2, pass2)) throw new IllegalArgumentException("Error al conectar" + tipo2)
                  //println("URL: " + url2 + "user2 " + user2 + "pass2 " + pass2)
                  //println("datos ruta ori: " + textfieldrutaor.text + "ruta des" + textfieldrutades.text)
                  MigrationService.FStoCS(reposfs, textfieldrutaor.text, reposorac, textfieldrutades.text)
                  ExitoCSCSDialog()
                  sys.exit()
                } catch {

                  case a: IllegalArgumentException => {
                    println("Error al ")
                    a.printStackTrace()
                    ErroronnetDialog()
                  }

                  case e: NullPointerException => {
                    println("ACA vemos que ec")
                    e.printStackTrace()

                    println("ACA vemos que ec")
                    ErrorNullQuery()
                    //	ErrorFSCSDialog()

                  }

                  case _ => {

                    ErrorCSCSDialog()
                    sys.exit()

                  }

                }

              }

              if (tipo2 == "Documentum") {

                try {
                  //var reposiemc: EmcRepository = new EmcRepository
                  if (!reposemc.connect(url, user, pass)) throw new IllegalArgumentException("Error al conectar" + tipo)
                  //println("datos user1: " + user + "pass1: " + pass)
                  MigrationService.CStoFS(reposemc, query, null, textfieldori3.text)
                  //println("datos query: " + query + "ruta ori" + textfieldori3.text)

                  //println("datos user2: " + user2 + "pass2: " + pass2)
                  if (!reposemc.connect(url2, user2, pass2)) throw new IllegalArgumentException("Error al conectar" + tipo2)

                  //	println("datos ruta ori: " + textfieldrutaor.text + "ruta des" + textfieldrutades.text)
                  MigrationService.FStoCS(reposfs, textfieldrutaor.text, reposemc, textfieldrutades.text)
                  ExitoDialog()
                  sys.exit()
                } catch {

                  case a: IllegalArgumentException => {
                    println("Error al ")
                    a.printStackTrace()
                    ErroronnetDialog()
                  }

                  case e: NullPointerException => {
                    println("ACA vemos que ec")
                    e.printStackTrace()
                    //println("ACA vemos que ec")
                    ErrorNullQuery()
                    //	ErrorFSCSDialog()

                  }

                  case _ => {

                    ErrorCSCSDialog()
                    sys.exit()

                  }

                }

              }
            }

          }
        }

        case _ => ()

      }

    }

    private def ErrorDatDialog() {
      Dialog.showMessage(parent = null,
        title = "Error en la Migracion",
        message = "Error en la migracion" + tipo) match {
          case Dialog.Result.Ok => {
            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def ErrorNullQuery() {
      Dialog.showMessage(parent = null,
        title = "Error en la Migracion",
        message = "El query ingresado NO contiene resultados") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }

          case _ => ()
        }
    }

    def ExitoDialog() {

      Dialog.showMessage(parent = null,
        title = "Exito en la Migracion",
        message = "La migracion" + tipo + option + "fue exitosa") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
      progressBar.visible = false
    }

    private def ErrorCSCSDialog() {
      Dialog.showMessage(parent = null,
        title = "Error en la Migracion de CS a CS",
        message = "Error en la migracion de " + tipo + "a" + tipo2) match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def ErrorFSCSDialog() {
      Dialog.showMessage(parent = null,
        title = "Error en la Migracion" + option,
        message = "Error en la migracion de " + tipo) match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def ErroronnetDialog() {
      Dialog.showMessage(parent = null,
        title = "Error en la Migracion" + option,
        message = "No fue posible conectar con el repositorio de tipo:  " + tipo + "\n") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //	RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def ExitoCSCSDialog() {
      Dialog.showMessage(parent = null,
        title = "Exito en la Migracion",
        message = "La migracion" + tipo + "a" + tipo2 + "fue exitosa") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //		RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def incompleteDatDialog() {
      Dialog.showMessage(parent = null,
        title = "Datos incompletos",
        message = "Favor de llenar  todos los datos") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    private def helpDialog() {
      Dialog.showMessage(parent = null,
        title = "Ayuda",
        message = "1. Eliga el tipo de migracion" + "\n" +
          "2. Eliga Origen y/o Destino " + "\n" +
          "3. Escriba su query" + "\n" +
          "4  Escriba su lista de atributos (de ser nesario)") match {
          case Dialog.Result.Ok => {

            //RepositoriesView.top.visible=true
            frame.close()
            //

            //RepositoriesView.top.visible=true

          }
          case _ => ()
        }
    }

    val migrator = new MigratorActor()
    migrator.start()

    implicit val intermediator = Intermediator(migrator)

    listenTo(intermediator)
    reactions += {
      case Receive(value) => {
        println(value)
        ExitoDialog()
      }
    }
  }

  def fillAtribList() {

    var cadenas = atrilist.split(',')

    for (i <- 0 to (cadenas.length - 1)) {
      println(cadenas(i))

      atributeList = atributeList :+ cadenas(i)
    }

  }

  top.visible = true
}