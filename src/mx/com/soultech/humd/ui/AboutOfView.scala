package mx.com.soultech.humd.ui

import scala.swing._
import javax.swing.ImageIcon
import swing._
import event._


object AboutOfView extends SimpleSwingApplication {
  
  
  
  val top = new MainFrame {
    
    frame=>
    title = "Acerca De... "
    
    iconImage = toolkit.getImage("imagen/logocorto.png")
      size = new Dimension(150, 150)
      visible = true
    
     val etiq1=new Label(" Created for :")
     val etiq2=new Label("HUMD")
     val etiq5=new Label("Developed for:")
     val etiq3=new Label(" los J... :")
     val etiq4=new Label("Version: 0.0.0.1 Beta")
     val im=new Label("Herramieta para migracion documental",new ImageIcon("imagen/logo.png"),Alignment.Center)
    
 
    
    
 
    contents = new BoxPanel(Orientation.Vertical){
     
     contents += new BoxPanel(Orientation.Horizontal) {
    	 border = Swing.EmptyBorder(20,10, 10, 30)
         contents+=etiq1
      
      
      }
     contents += new BoxPanel(Orientation.Horizontal) {
    	 	border = Swing.EmptyBorder(20,10, 10, 30)
    	 	contents+= im
      
      }
      
      contents += new BoxPanel(Orientation.Horizontal) {
				border = Swing.EmptyBorder(20,10, 10, 30)
				contents+=etiq2
      
      
      }
      
      contents += new BoxPanel(Orientation.Horizontal) {
				border = Swing.EmptyBorder(20,10, 10, 30)
				contents+=etiq5
				contents += Swing.HStrut(30)
				contents+=etiq3
      
      }
      
      
      
      border = Swing.EmptyBorder(170, 170, 170, 170)
    }
    
  
   override def closeOperation() { 
           /// HumdView.top.visible=true
            frame.close() }
  
  
  }


  top.visible=true

}
