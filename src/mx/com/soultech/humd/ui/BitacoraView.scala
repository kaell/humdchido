package mx.com.soultech.humd.ui

import java.awt.Dimension
import java.awt.event.{ MouseEvent, MouseAdapter }
import swing._
import swing.event._
import event.TableColumnsSelected
import mx.com.soultech.humd.utils.UtilFile
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
import java.io.File

object BitacoraView extends SimpleSwingApplication {

  
  var ren = 0
  var col = 0
  val headers = Array("Nombre", "Tipo")
  val seebutton = new Button("Ver")
  UtilFile.listaArchivos()
  val rowData = UtilFile.myArrayBit
  val exitbutton = new Button("Salir")
  val table = new Table(rowData.asInstanceOf[Array[Array[Any]]], headers)
  val label = new Label("")

  val ui = new BoxPanel(Orientation.Vertical) {

    val header = table.peer.getTableHeader()

    contents += new BorderPanel {

      add(seebutton, BorderPanel.Position.West)

    }

    header.addMouseListener(new MouseAdapter() {
      override def mouseClicked(e: MouseEvent) {
        val col = header.columnAtPoint(e.getPoint())
        //val col2= header.columnSelectionChanged(col.toString)
        //label.text = "Column header "+col2+" selected"
        label.text = "Column header " + col + " selected"

      }
    })

    listenTo(table.selection)

    reactions += {
      //case TableUpdated(_, r, i) => println(r +" "+ i)}

      case TableColumnsSelected(_, r, i) =>

        ren = table.selection.rows.leadIndex
        col = table.selection.columns.leadIndex

        println("Renglon: %s; Columns: %s\n" format (ren, col))

        if (ren >= 0 && col >= 0) {
          table.peer.getDefaultEditor(classOf[Object])
          println("Haz Seleccionado el :" + rowData(ren)(col))
        }

    }
    contents += new ScrollPane(table)

    reactions += {
      case TableColumnsSelected(_, r, i) =>
        label.text = "Column " + table.selection.columns.leadIndex + " selected, range = " + r + "," + i
    }
    contents += new ScrollPane(table)
    contents += label

    contents += new BorderPanel {

      add(exitbutton, BorderPanel.Position.East)
    }

    border = Swing.EmptyBorder(40, 50, 20, 50)

  }

  val top = new MainFrame { frame=>
    title = "Bitacora"
    preferredSize = new Dimension(600, 460)
    iconImage = toolkit.getImage("imagen/logocorto.png")
    size = new Dimension(200, 200)
    visible = true

    contents = ui

    listenTo(seebutton)
    reactions += {
      case ButtonClicked(`seebutton`) => {

        OpenFiles()
      }

    }

    listenTo(exitbutton)
    reactions += {
      case ButtonClicked(`exitbutton`) => showCloseDialog()

    }

    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    def showCloseDialog() {
      Dialog.showConfirmation(parent = null,
        title = "Salir",
        message = "�Esta seguro que quiere salir de Bitacora?") match {
          case Dialog.Result.Ok =>{ 
            
            frame.close()
            //HumdView.top.visible=true
          }
          case _ => ()
        }
    }

  }

  private def OpenFiles() {

    ren = table.selection.rows.leadIndex
    col = table.selection.columns.leadIndex
    if (col > 0) {
      col = 0
    }
    UtilFile.openFile(rowData(ren)(col))
  }

  top.visible = true

}