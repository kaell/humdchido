package mx.com.soultech.humd.ui

import java.awt.Dimension
import java.awt.event.{ MouseEvent, MouseAdapter }
import swing._
import swing.event._
import event.TableColumnsSelected
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
import swing._
import swing.event.{ TableRowsSelected, TableEvent, TableColumnsSelected, ButtonClicked }
import mx.com.soultech.humd.utils.FillTables
import mx.com.soultech.humd.utils.UtilFile
import mx.com.soultech.humd.impl.OracleRepository
import mx.com.soultech.humd.impl.EmcRepository
import mx.com.soultech.humd.utils.FillCombo
//case class TableColumnHeaderSelected(override val source:Table, column: Int) extends TableEvent(source)

object RepositoriesView extends SimpleSwingApplication {

  
  
  FillTables.llenaTablas()
  var ren = 0
  var col = 0
  var delete = false
  val headers = Array("Nombre", "Tipo")
  val testbutton = new Button("Probar")
  val chancebutton = new Button("Cambiar")
  val listrepos = FillCombo.listarepos
  val rowData = FillTables.myArray
  val addbutton = new Button("Agregar")
  val exitbutton = new Button("Salir")
  val deletebutton = new Button("Borrar")
  val table = new Table(rowData.asInstanceOf[Array[Array[Any]]], headers)
  val label = new Label("")

  val ui = new BoxPanel(Orientation.Vertical) {

    border = Swing.EmptyBorder(20, 10, 10, 30)
    val header = table.peer.getTableHeader()

    contents += new BoxPanel(Orientation.Horizontal) {
      border = Swing.EmptyBorder(20, 10, 10, 30)
      contents += testbutton
      contents += Swing.HStrut(20)
      contents += addbutton
      contents += Swing.HStrut(20)
      contents += deletebutton
      contents += Swing.HStrut(20)
      contents += chancebutton
      contents += Swing.HStrut(20)
      contents += exitbutton
      contents += Swing.HStrut(20)

    }

    header.addMouseListener(new MouseAdapter() {
      override def mouseClicked(e: MouseEvent) {
        val col = header.columnAtPoint(e.getPoint())
        //val col2= header.columnSelectionChanged(col.toString)
        //label.text = "Column header "+col2+" selected"
        label.text = "Column header " + col + " selected"

      }
    })

    listenTo(table.selection)

    reactions += {
      //case TableUpdated(_, r, i) => println(r +" "+ i)}

      case TableColumnsSelected(_, r, i) =>

        ren = table.selection.rows.leadIndex
        col = table.selection.columns.leadIndex
        if (col > 0) {
      col = 0
        }
        

        println("Renglon: %s; Columns: %s\n" format (ren, col))
        println("Sin Formato renglon: \n"+ren+"Columna"+col)
        
        if (ren >= 0 && col >= 0) {
          
          table.peer.getDefaultEditor(classOf[Object])
          
          println("Haz Seleccionado el :" + rowData(ren)(col))
        }

    }
    
    /* reactions += {
      case TableColumnsSelected(_, r, i) =>
        label.text = "Column " + table.selection.columns.leadIndex + " selected, range = " + r + "," + i
    }*/
    
    
    
    
    contents += new ScrollPane(table)
    contents += label

    //border = Swing.EmptyBorder(40, 50, 20, 50)

  }

  val top = new MainFrame { frame=>

    title = "Repositorios"
    preferredSize = new Dimension(540, 680)
    iconImage = toolkit.getImage("imagen/logocorto.png")
    size = new Dimension(200, 200)

    contents = ui

    listenTo(addbutton)
    reactions += {
      case ButtonClicked(`addbutton`) => {
    	  		val newrep=NewRepositoryView
    	  	        newrep.top.visible=true 			
                     
    	  	        frame.close
    		
      
      					}
    	
      
      
      
    }

    listenTo(testbutton)
    reactions += {
      
         
    
    case ButtonClicked(`testbutton`) =>
        
     ren = table.selection.rows.leadIndex
     col = table.selection.columns.leadIndex
      //label.text =("Probando Conexion de"+ rowData(ren)(col)+"....")
     if (col > 0) {
      col = 0
    } 
      
     
     if (ren >= 0 && col >= 0) {
          table.peer.getDefaultEditor(classOf[Object])
          println("Haz Seleccionado el :" + rowData(ren)(col))
        }

          
         TestDialog()		 
         	
    }

    listenTo(chancebutton)
    reactions += {
      case ButtonClicked(`chancebutton`) => {
        showChanceDialog()
        NewRepositoryView
      }
    }

    listenTo(deletebutton)
    reactions += {
      case ButtonClicked(`deletebutton`) => {

        showDeleteDialog()
        
      
      }

      case _ =>

    }

    listenTo(exitbutton)
    reactions += {
      case ButtonClicked(`exitbutton`) =>

        showCloseDialog()

      case _ =>

    }

    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    def showCloseDialog() {
      Dialog.showConfirmation(parent = null,
        title = "Cancelar",
        message = "�Quiere salir de Repositorios?") match {
          case Dialog.Result.Ok => {
            
            //HumdView.top.visible=true
            frame.close()
        	  						
          }

          case _ => ()
        }
    }

    var cad = rowData(ren)(col)

    private def showDeleteDialog() {

      ren = table.selection.rows.leadIndex
      col = table.selection.columns.leadIndex
      if (col > 0) {
      col = 0
    }
      
      Dialog.showConfirmation(parent = null,
        title = "Borrar",
        message = "Esta Seguro que quiere Borrar el repositorio: " + rowData(ren)(col)) match {

          case Dialog.Result.Ok => {

            val fil = UtilFile
            println("Este es la cadena a borrar: " + rowData(ren)(col).toString)
            fil.removeLineFromFile("imagen/prueba.txt", rowData(ren)(col).toString)
            //println("Este es la cadena a borrar: "+rowData(ren)(col).toString)
            //dispose() 
           
            //table.update(ren, col)
            //table.updateCell(ren, col)
            //frame.close
             sys.exit(0)
          }
          case _ => ()

        }

    }

     def showChanceDialog() {

      ren = table.selection.rows.leadIndex
      col = table.selection.columns.leadIndex
     
      
      Dialog.showConfirmation(parent = null,
        title = "Borrar",
        message = "Esta Seguro que quiere Cambiar los datos  del repositorio: " + rowData(ren)(col)) match {

          case Dialog.Result.Ok => {

            val fil = UtilFile
            println("Este es la cadena a borrar: " + rowData(ren)(col).toString)
            fil.removeLineFromFile("imagen/prueba.txt", rowData(ren)(col).toString)
            //println("Este es la cadena a borrar: "+rowData(ren)(col).toString)
            //dispose() 

            sys.exit(0)
           //frame.close
          
          
          }
          case _ => ()

        }

    }

  
  def TestDialog() {

      
	  ren = table.selection.rows.leadIndex
      col = table.selection.columns.leadIndex
     
      if (col > 0) {
      col = 0
    }
      
      
      
      var repos=rowData(ren)(col).toString()
      println("ESTE Es el repos"+repos)
      
      var indice = listrepos.indexOf(repos)
      //println("Lista "+listrepos(0))
      //println("indice..."+indice)
      var tipo = FillTables.myArray(indice)(1)
      var user = FillTables.myArray(indice)(4)
      var pass = FillTables.myArray(indice)(5)
      var url = FillTables.myArray(indice)(2) + ":" + FillTables.myArray(indice)(3)
      var conex=""
      if(tipo=="Oracle"){
    	  
         var reposiOra: OracleRepository = new OracleRepository
         url = url+"/cs/idcplg"
        
         
         if(!reposiOra.connect(url, user, pass))
           
           conex="NO EXITOSA"
           else
            conex="EXITOSA" 
             
     }
      
      
      if(tipo=="Documentum"){
        var reposemc: EmcRepository = new EmcRepository
        if(reposemc.connect(url, user, pass))
          conex="EXITOSA"
        else
            conex="NO EXITOSA" 		
      
      }
      
      
      
      
      
      
      Dialog.showMessage(parent = null,
        title = "Probar",
        message = "La conexion de : " + rowData(ren)(col)+"se realizo de manera..."+conex) match {

          
        
        case Dialog.Result.Ok => {

          

           frame.close()

          }
          case _ => ()

        }

    }

  
  
  
  
  
  
  }
  
  top.visible = true

}