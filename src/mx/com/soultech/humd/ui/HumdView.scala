package mx.com.soultech.humd.ui

import scala.swing._
import swing._
import event._
import javax.swing.ImageIcon
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE
//import mx.com.soultech.humd.ui.RepositoriesView

object HumdView extends SimpleSwingApplication {

  var top = new MainFrame {
    
    title = "Herramienta para migracion documental "
    iconImage = toolkit.getImage("imagen/logocorto.png")
    size = new Dimension(300, 300)
    
    //visible = true

    val salirAccion = Action("Salir") {
      showCloseDialog()
    }

    val migraAccion = Action("Migrar") {

      val mig = MigrationView
          mig.top.visible=true		  
    }

    val repositorioAccion = Action("Repositorios") {

      var rep =  RepositoriesView
          rep.top.visible=true

    }

    val bitacoraAccion = Action("Bitacora") {

      val pantabita = BitacoraView
    	  pantabita.top.visible=true	  
    }

    /*val documentacionAccion = Action("Documentacion") {
      showCloseDialog()
    }*/

    val acercadeAccion = Action("Acerca de") {

      val pantac = AboutOfView
          pantac.top.visible=true
    }

    menuBar = new MenuBar {

      contents += new Menu("Acciones") {
        contents += new MenuItem(migraAccion)
        contents += new MenuItem(salirAccion)
      }
      contents += new Menu("Configuracion") {

        contents += new MenuItem(repositorioAccion)
        contents += new MenuItem(bitacoraAccion)

      }

      contents += new Menu("Ayuda") {

        //contents += new MenuItem(documentacionAccion)
        contents += new MenuItem(acercadeAccion)

      }

    }

    contents = new FlowPanel {

      contents += new Label("Herramieta para migracion documental", new ImageIcon("imagen/logo.png"), Alignment.Leading)

      border = Swing.EmptyBorder(200, 200, 150, 150)
    }

    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    private def showCloseDialog() {
      Dialog.showConfirmation(parent = null,
        title = "Exit",
        message = "�Esta seguro que quiere salir de HUMD?") match {
          case Dialog.Result.Ok => sys.exit(0)
          case _ => ()
        }
    }

  }

  top.visible=true

}
