package mx.com.soultech.humd.ui

import scala.swing._
import javax.swing.ComboBoxEditor
import scala.swing.ComboBox
import scala.swing.event
import swing.event._
import scala.io._
import java.io._
import event._
import javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE

object NewRepositoryView extends SimpleSwingApplication {

  //RepositoriesView.top.dispose()
  val top = new MainFrame { frame=>

    title = "Agregar Repositorio"
    preferredSize = new Dimension(600, 460)

    iconImage = toolkit.getImage("imagen/logocorto.png")
    size = new Dimension(200, 200)
    visible = true

    contents = new BoxPanel(Orientation.Vertical) {

    
      val botoncancelar = new Button("Cancelar") {

      }
      val botonguardar = new Button("Guardar") {

      }
      val combo = new ComboBox(Seq("Documentum", "Oracle"))
      val etiqtipom = new Label("1. Nombre del Repositorio: ")
      val etiqtipo = new Label("Tipo: ")
      val etiqeorin2 = new Label("Origen:  ")
      val fieldurl = new TextField {
        text = ""
        columns = 15
      }

      val namerep = new TextField {
        text = ""
        columns = 15
      }
      val fieldport = new TextField {
        text = ""
        columns = 15
      }
      val fielduser = new TextField {
        text = ""
        columns = 15
      }
      val fieldpass = new TextField {
        text = ""
        columns = 15
      }
      val etiqdatcon = new Label("Datos de Conexion: ")
      val etiqdestino2 = new Label("Destino: ")
      val etiqdat = new Label("Datos del Repositorio ")
      val fieldquery = new TextField("Escriba su Query...    ")
      val etiqquery = new Label("Query: ")
      val etiqrutas = new Label("\n\n\n3. Escriba las Rutas\n\n ")

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(10, 10, 10, 30)
        contents += etiqdat

      }

      //contents += new BorderPanel {
      //add(etiqdat, BorderPanel.Position.North)

      //} 

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)
        contents += new Label("1. Nombre del Repositorio: ")
        contents += Swing.HStrut(5)
        contents += namerep
      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)
        contents += etiqtipo
        contents += Swing.HStrut(5)
        contents += combo

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(10, 10, 10, 30)
        contents += etiqdatcon

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)

        contents += Swing.HStrut(5)
        contents += new Label("URL:")
        contents += Swing.HStrut(5)
        contents += fieldurl
        contents += Swing.HStrut(5)
        contents += new Label("Puerto:")
        contents += Swing.HStrut(5)
        contents += fieldport
        contents += Swing.HStrut(5)

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)

        contents += Swing.HStrut(5)
        contents += new Label("Usuario:")
        contents += Swing.HStrut(5)
        contents += fielduser
        contents += Swing.HStrut(5)
        contents += new Label("Password:")
        contents += Swing.HStrut(5)
        contents += fieldpass

      }

      contents += new BoxPanel(Orientation.Horizontal) {
        border = Swing.EmptyBorder(20, 10, 10, 30)
        
        contents += Swing.HStrut(40)
        contents += botoncancelar
        contents += Swing.HStrut(40)
        contents += botonguardar
        contents += Swing.HStrut(40)

      }

      listenTo(botonguardar)
      reactions += {

        case ButtonClicked(`botonguardar`) =>

          val name: String = namerep.text
          val url: String = fieldurl.text
          val user: String = fielduser.text
          val pass: String = fieldpass.text
          val port: String = fieldport.text
          val tipo: String = combo.item

          if (name == "" || url == "" || user == null || pass == "" || pass == null || port == "" || tipo == null) {
            incompleteDatDialog()

            //sys.exit(0)
          }

          else{
            saveDates(name, url, user, pass, port, tipo)
            Dialog.showMessage(parent = null,
            title = "Guardar",
            message = "El repositorio fue guardado") match {
              case Dialog.Result.Ok =>
                
                sys.exit()
                
              case _ => ()
            }

           sys.exit(0)
           
          }

      } 
     listenTo(botoncancelar)
      reactions += {
        case ButtonClicked(`botoncancelar`) => frame.close()

      }

      //listenTo(namerep)
      //reactions += {
      //case EditDone(`namerep`) =>

      //}

      //case EditDone(fieldurl) =>
      //var url=fieldurl.text 
      //case EditDone(fieldport) =>
      //var port=fieldport.text 
      //case EditDone(fielduser) =>
      //var user=fielduser.text 
      //case EditDone(fieldpass) =>
      //var pass=fieldpass.text 

      border = Swing.EmptyBorder(60, 80, 100, 100)
    }

    private def saveDates(name: String, url: String, user: String, pass: String, port: String, tipo: String) {

      val fw = new File("imagen/prueba.txt")

      val reader = new BufferedReader(new FileReader("imagen/prueba.txt"))

      val writer = new FileWriter(fw, true)

      //writer.write("\n")
      writer.write(name)
      writer.write(",")
      writer.write(tipo)
      writer.write(",")
      writer.write(url)
      writer.write(",")
      writer.write(port)
      writer.write(",")
      writer.write(user)
      writer.write(",")
      writer.write(pass)
      writer.write("\n")
      writer.close()
      
     
    
    }

    peer.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)

    override def closeOperation() { showCloseDialog() }

    private def showCloseDialog() {
      Dialog.showConfirmation(parent = null,
        title = "Cancelar",
        message = "�Quiere salir de Agregar Repositorio?") match {
          case Dialog.Result.Ok => {
            //RepositoriesView.top.visible=true
        	
            frame.close()
        	
        	}
          case _ => ()
        }
    }

    private def showSaveDialog() {

    }

    private def incompleteDatDialog() {
      Dialog.showMessage(parent = null,
        title = "Datos incompletos",
        message = "Favor de llenar  todos los datos") match {
          case Dialog.Result.Ok => {
            
        	//RepositoriesView.top.visible=true
            //sys.exit
            frame.close() 
           //RepositoriesView.top.visible=true
          
          
          }
          case _ => ()
        }
    }

  }

  top.visible = true
}



















